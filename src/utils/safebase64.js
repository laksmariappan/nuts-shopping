/**
 * web safe base64 encode JSON object
 *
 * @param {Object} data
 * @returns {String} encodedData
 */
export const encode = (data) => {
    let encodedData = JSON.stringify(data)
    encodedData = window.btoa(encodedData)
      .replace(/\+/g, '-') // Convert '+' to '-'
      .replace(/\//g, '_') // Convert '/' to '_'
      .replace(/=+$/, '') // Remove ending '='
  
    return encodedData
  }
  
  /**
   * web safe base64 decode string into JSON object
   *
   * @param {String} data
   * @returns {Object} decodedData
   */
  export const decode = (data) => {
    const decodedData = data
      .replace(/-/g, '+') // Convert '-' to '+'
      .replace(/_/g, '/') // Convert '_' to '/'
  
    // FIXME: handle parse errors
    return JSON.parse(window.atob(decodedData))
  }
  