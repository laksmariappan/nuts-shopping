import axios from 'axios'

/**
 * Global XHR headers.
 *
 * @type {Object}
 */
let headers = {
  'Content-Type': 'application/json'
}

/**
 * Axios request middleware
 */
axios
  .interceptors
  .request
  .use(config => ({ ...config, headers }))


const api = {

  /**
   * @type {Object}
   */
  get headers() {
    return headers
  },

  /**
   * @type {Object}
   */
  set headers(newHeaders) {
    headers = { ...headers, ...newHeaders }
  },

  /**
   * Creates a POST request.
   *
   * @param {String} endpoint
   * @param {Object} body
   * @returns {Promise<Object>}
   */
  post(endpoint, body, config) {
    return axios
      .post(endpoint, body, config)
  },

  /**
   * Creates a GET request.
   *
   * @param {String} endpoint
   * @returns {Promise<Object>}
   */
  get(endpoint, config) {
    return axios
      .get(endpoint, {
        // Need to pass `data` as an empty object, or else axios will strip
        // the Content-Type header from the GET request. See:
        // https://github.com/axios/axios/issues/86#issuecomment-139638284
        data: {},
        ...config
      })
  },

  /**
   * Creates a DELETE request.
   *
   * @param {String} endpoint
   * @returns {Promise<Object>}
   */
  delete(endpoint, config) {
    return axios
      .delete(endpoint, config)
  }
}

export default api
