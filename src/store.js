import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import api from '@/utils/api'

/**
 * @fileoverview This is the index.js for Vuex, our state manager where we
 * have our top level state object and all of our dependent state modules.
 * Most of the interesting stuff in the application will be happening here.
 * This is where we have our top level getters, actions, and mutations.
 * If you want to know what the application can do, this is a decent place
 * to start. Also check App.vue to see how we get some of our initial data.
 *
 * @note actions, getters, and mutations exported separately for unit testing
 */

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    api_url: 'https://api.commercetools.co/nuts-custom-demo-1/products?',
    offset: 0,
    limit: 10,
    total: 0,
    jwt: 'TcTN3-eZFJio4tGtFKYc2vBnNT9sdoGG'
  },
  mutations: {
    setOffset(state) {
      state.offset = state.products.length + 1
    },
    setTotal(state, n) {
      state.total = n
    },
    updateProducts(state, n) {
      state.products.push(n)
    }
  },
  actions: {
    init() {
      api.headers = {
        Authorization: `Bearer ${this.state.jwt}`
      }
    },
    loadMoreProducts() {
      if (this.state.products.length && this.state.products.length === this.state.total) return
      const apiURL = `${this.state.api_url}offset=${this.state.offset}&limit=${this.state.limit}`
      axios.get(apiURL)
        .then((response) => {
          const resData = response.data
          this.commit('setTotal', resData.total)
          resData.results.forEach((item) => {
            const curData = item.masterData.current
            const curVariant = item.masterData.current.masterVariant
            const getBadgeFlag = () => {
              const badgeArr = curVariant.attributes.filter(attr => attr.name === 'Organic')
              return (badgeArr.length) ? badgeArr[0].value : false
            }
            // const getProdPrice = () => {
            // }
            const product = {
              badge: getBadgeFlag(),
              description: curData.description.en,
              imageURL: curVariant.images.length ? curVariant.images[0].url : '',
              id: item.id,
              name: curData.name.en,
              price: curVariant.prices.length ? curVariant.prices[0].value.centAmount : 0
            }
            this.commit('updateProducts', product)
          })
          this.commit('setOffset')
        })
    }
  }
})
