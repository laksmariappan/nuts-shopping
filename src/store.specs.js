import Vuex from 'vuex'
import store from '@src/store'

jest.mock('./persistedState', () => {})

afterEach(() => jest.resetAllMocks())

describe('store', () => {
  it('should return a Vuex Store', () => {
    expect(store instanceof Vuex.Store).toBe(true)
  })
})

/***********/
// ACTIONS //
/***********/

describe('init', () => {
  beforeEach(() => {
  })
})


/*************/
// MUTATIONS //
/*************/
describe('Mutations', () => {    
})
